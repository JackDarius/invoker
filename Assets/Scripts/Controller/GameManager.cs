using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.Model;

namespace Assets.Scripts.Controller
{
    public class GameManager : MonoBehaviour
    {
        public GameObject RoomPrefab;
        public GameObject GobelinPrefab;
        public enum Topic { Mixed, JointMobility, Strengthening };
        public Topic topic;

        public int NbrRoom;
        public int NbrGobelin;

        private static GameManager _instance;
        private IKGameSession _ikGameSession;
        private IKExercice _ikExercice;

        public static GameManager Instance
        {
            get
            {
                if(_instance == null)
                {
                    Debug.LogError("Controller is not instanciated yet");
                    return null;
                }
                return _instance;
            }
        }

        void Start()
        {
            _ikExercice = new IKExercice();
            _instance = this;
            _ikGameSession = new IKGameSession();
            DrawLevel();
        }

        private void DrawLevel()
        {
            //Draw level and gobelins
            foreach(IKRoom r in _ikGameSession.Rooms)
            {
                GameObject roomGo = Instantiate(RoomPrefab) as GameObject;
                if (r.Gobelins.Count != 0)
                {
                    foreach (IKGobelin gob in r.Gobelins)
                    {
                        GameObject gobGo = Instantiate(GobelinPrefab) as GameObject;
                        gobGo.transform.parent = roomGo.transform;
                    }  
                }
            }
        }

        void Update()
        {
            if(Input.GetKey(KeyCode.RightArrow))
            {
                //TODO AFFICHER LA SALLE SUIVANTE
            }
        }
    }
}

