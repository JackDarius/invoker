using UnityEngine;
using System.Collections;

namespace Assets.Scripts.Model
{
    public abstract class IKCharacter
    {
        public enum State { dead, unconscious, stand };

        protected string _name;
        protected GameObject _gameObject;
        protected State _state;

        /* Constructor */
        public IKCharacter()
        {
        }

        public IKCharacter(string name, GameObject gameObject)
        {
            _state = State.stand;
            _name = name;
            _gameObject = gameObject;
        }

        /* Getter - Setter */
        public GameObject GameObject
        {
            get { return _gameObject; }
            set { _gameObject = value; }
        }

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        /* Features */
        public virtual void Die()
        {
            _state = State.dead;
        }
    }
}