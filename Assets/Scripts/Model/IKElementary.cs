using UnityEngine;
using System.Collections;

namespace Assets.Scripts.Model
{
    public class IKElementary : IKMonster
    {
        /* Constructor */
        public IKElementary(Element element, string name, GameObject gameObject)
            : base(element, name, gameObject)
        {
        }
    }
}