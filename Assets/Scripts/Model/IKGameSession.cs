using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.Controller;

namespace Assets.Scripts.Model
{
    public class IKGameSession
    {
        private IKExercice _ikExercice;
        private List<IKRoom> _rooms;
        private List<IKGobelin> _gobelins;

        /* Constructor */
        public IKGameSession()
        {
            _rooms = new List<IKRoom>();
            _gobelins = new List<IKGobelin>();
            AddRoomToGameSession();
            AddGobelinToGameSession();
            
            AddGobelinsToRoomRandomly();
        }

        /* Getter - Setter */
        public List<IKRoom> Rooms
        {
            get { return _rooms; }
            set { _rooms = value; }
        }

        /* Features */
        public void AddRoomToGameSession()
        {
            for (int i = 0; i < GameManager.Instance.NbrRoom; i++)
            {
                IKRoom room = new IKRoom();
                _rooms.Add(room);
            }
        }

        public void AddGobelinToGameSession()
        {
            for (int i = 0; i < GameManager.Instance.NbrGobelin; i++)
            {
                IKGobelin gobelin = new IKGobelin();
                _gobelins.Add(gobelin);
            }
        }

        public void AddGobelinsToRoomRandomly()
        {
            int compteur = 0;
            int nbrGobInRoom = 0;
            int nbrGobelins = _gobelins.Count;
            List<IKGobelin> gob = _gobelins;

            while (compteur < nbrGobelins)
            {
                foreach (IKRoom room in _rooms)
                {
                    if (compteur >= nbrGobelins) { return; }

                    System.Random rnd = new System.Random();

                    if (nbrGobelins - compteur >= 2)
                        nbrGobInRoom = rnd.Next(0, 3); //Random Integer from 0 to 2!!!
                    else
                        nbrGobInRoom = rnd.Next(0, (nbrGobelins - compteur) + 1);

                    for (int i = 0; i <= nbrGobInRoom - 1; i++)
                    {
                        room.AddGobelin(gob[gob.Count - 1]);
                        gob.RemoveAt(gob.Count - 1);

                        compteur++;
                    }
                }
            }


        }
    }
}