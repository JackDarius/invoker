using UnityEngine;
using System.Collections;

namespace Assets.Scripts.Model
{
    public class IKGobelin : IKMonster
    {
        private static int _counter = 0;
        private string _id;
        private float _attackDamage;

        /* Constructor */
        public IKGobelin()
        {
            _id = _counter.ToString();
            _counter++;
        }

        public IKGobelin(float attackDamage, Element element, string name, GameObject gameObject) : base(element, name, gameObject)
        {
            _attackDamage = attackDamage;
        }

        /* Getter - Setter */
        public string Id
        {
            get { return _id; }
            set { _id = value; }
        }

        /* Features */
        public override void Die()
        {
            base.Die();
            if (_gameObject != null)
                MonoBehaviour.Destroy(_gameObject);
        }

        public void AttackWizard(IKWizard wizard, float damage)
        {
            if (wizard.CurrentHealth > 0)
                wizard.CurrentHealth = wizard.CurrentHealth - _attackDamage;

            wizard.ChangeState();
        }
    }
}