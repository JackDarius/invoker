using UnityEngine;
using System.Collections;

namespace Assets.Scripts.Model
{
    public class IKMonster : IKCharacter
    {
        public enum Element { water, fire, plant };
        protected Element _element;

        /* Constructor */
        public IKMonster()
        { }

        public IKMonster(Element element, string name, GameObject gameObject)
            : base(name, gameObject)
        {
            _element = element;
        }

        /* Getter - Setter */

        /* Overload Operator */
        public static bool operator >(IKMonster monsterA, IKMonster monsterB)
        {
            return (monsterA._element == Element.water && monsterB._element == Element.fire) || (monsterA._element == Element.fire && monsterB._element == Element.plant) ||
                   (monsterA._element == Element.plant && monsterB._element == Element.water);
        }

        public static bool operator <(IKMonster monsterA, IKMonster monsterB)
        {
            return (monsterA._element == Element.fire && monsterB._element == Element.water) || (monsterA._element == Element.plant && monsterB._element == Element.fire) ||
                   (monsterA._element == Element.water && monsterB._element == Element.plant);
        }

        /* Features */
        public void AttackMonster(IKMonster opponentMonster)
        {
            if (_element == opponentMonster._element) { return; }

            if (this > opponentMonster)
                opponentMonster.Die();
            else if (this < opponentMonster)
                this.Die();
        }
    }

}
