using UnityEngine;
using System.Collections;

namespace Assets.Scripts.Model
{
    public class IKWizard : IKCharacter
    {
        public enum Type { beginner, intermediary, hard };
        protected Type _type;
        protected float _currentHealth;
        protected float _maxHealth;

        /* Constructor */
        public IKWizard(Type type, string name, GameObject gameObject)
            : base(name, gameObject)
        {
            _type = type;
        }

        /* Getter - Setter */
        public float CurrentHealth
        {
            get { return _currentHealth; }
            set { _currentHealth = value; }
        }

        public float MaxHealth
        {
            get { return _maxHealth; }
            set { _maxHealth = value; }
        }

        /* Features */
        public void TakeDamage(float damage)
        {
            _currentHealth = _currentHealth - damage;
        }

        public void RegenerateHealth(float healing)
        {
            if (_currentHealth < _maxHealth)
            {
                if ((_currentHealth + healing) >= _maxHealth)
                    _currentHealth = _maxHealth;
                else
                    _currentHealth = _currentHealth + healing;
            }
        }

        public void ChangeState()
        {
            if (_currentHealth == 0)
                _state = State.dead;
            if (_currentHealth == _maxHealth / 2f)
                _state = State.unconscious;
        }
    }
}