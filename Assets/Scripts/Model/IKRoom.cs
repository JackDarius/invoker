using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Assets.Scripts.Model
{
    public class IKRoom
    {
        private static int _counter = 0;
        private string _id;
        private List<IKGobelin> _gobelins;

        /* Constructor */
        public IKRoom()
        {
            _gobelins = new List<IKGobelin>();
            _id = _counter.ToString();
            _counter++;
        }

        /* Getter - Setter */
        public string Id
        {
            get { return _id; }
            set { _id = value; }
        }

        public List<IKGobelin> Gobelins
        {
            get { return _gobelins; }
            set { _gobelins = value; }
        }

        /* Features */
        public void AddGobelin(IKGobelin gob)
        {
            _gobelins.Add(gob);
        }

        public void RemoveThisGobelin(IKGobelin gob)
        {
            _gobelins.RemoveAt(_gobelins.IndexOf(gob));
            //TODO REMOVEFROMSCENE
        }
    }
}